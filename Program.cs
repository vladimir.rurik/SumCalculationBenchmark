﻿using System;
using System.Linq;
using System.Threading;
using System.Diagnostics;
using System.Collections.Generic;
using NickStrupat;

class Program
{
    static void Main()
    {
        var computerInfo = new ComputerInfo();
        string cpuInfo = GetCpuInfo();
        ulong totalPhysicalMemory = computerInfo.TotalPhysicalMemory;
        string osVersion = Environment.OSVersion.ToString();

        Console.WriteLine($"CPU: {cpuInfo}");
        Console.WriteLine($"Total Physical Memory: {totalPhysicalMemory} bytes");
        Console.WriteLine($"OS Version: {osVersion}");

        int[] array100k = GenerateArray(100_000);
        int[] array1m = GenerateArray(1_000_000);
        int[] array10m = GenerateArray(10_000_000);

        BenchmarkSumMethods(array100k, 100_000);
        BenchmarkSumMethods(array1m, 1_000_000);
        BenchmarkSumMethods(array10m, 10_000_000);
    }

    static string GetCpuInfo()
    {
        string cpuInfo = string.Empty;
        using (var searcher = new System.Management.ManagementObjectSearcher("select * from Win32_Processor"))
        {
            foreach (var item in searcher.Get())
            {
                cpuInfo = item["Name"].ToString();
                break; // Assuming one CPU
            }
        }
        return cpuInfo;
    }

    static int[] GenerateArray(int size)
    {
        var random = new Random();
        return Enumerable.Range(0, size).Select(x => random.Next(100)).ToArray();
    }

    static long SumSequential(int[] array)
    {
        long sum = 0;
        foreach (var num in array)
        {
            sum += num;
        }
        return sum;
    }

    static long SumParallelThreads(int[] array)
    {
        long sum = 0;
        int threadCount = Environment.ProcessorCount;
        int size = array.Length / threadCount;
        var threads = new List<Thread>();

        for (int i = 0; i < threadCount; i++)
        {
            int start = i * size;
            int end = (i == threadCount - 1) ? array.Length : start + size;
            var thread = new Thread(() =>
            {
                for (int j = start; j < end; j++)
                {
                    Interlocked.Add(ref sum, array[j]);
                }
            });
            threads.Add(thread);
            thread.Start();
        }

        foreach (var thread in threads)
        {
            thread.Join();
        }

        return sum;
    }

    static long SumParallelLinq(int[] array)
    {
        return array.AsParallel().Sum(x => (long)x);
    }

    static void BenchmarkSumMethods(int[] array, int arrayLength)
    {
        var stopwatch = new Stopwatch();

        stopwatch.Start();
        var seqSum = SumSequential(array);
        stopwatch.Stop();
        Console.WriteLine($"Array Length: {arrayLength}, Sequential Sum: {seqSum}, Time: {stopwatch.ElapsedMilliseconds} ms");

        stopwatch.Restart();
        var parSum = SumParallelThreads(array);
        stopwatch.Stop();
        Console.WriteLine($"Array Length: {arrayLength}, Parallel Sum (Threads): {parSum}, Time: {stopwatch.ElapsedMilliseconds} ms");

        stopwatch.Restart();
        var linqSum = SumParallelLinq(array);
        stopwatch.Stop();
        Console.WriteLine($"Array Length: {arrayLength}, Parallel Sum (LINQ): {linqSum}, Time: {stopwatch.ElapsedMilliseconds} ms");
    }
}
