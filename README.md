# SumCalculationBenchmark

## Project Overview

`SumCalculationBenchmark` is a C# application designed to benchmark the performance of sequential and parallel sum calculation methods on arrays of integers. The project compares traditional loop-based summation, parallel processing using threads, and parallel LINQ methods.

## Benchmark Environment

- **CPU**: Intel(R) Core(TM) i9-10940X CPU @ 3.30GHz
- **Total Physical Memory**: 64 GB
- **OS Version**: Microsoft Windows NT 10.0.22621.0

## Benchmark Results

The following table summarizes the time taken to calculate the sum of integers in an array using different methods and array sizes:

| Array Length | Method                 | Sum         | Time (ms) |
|--------------|------------------------|-------------|-----------|
| 100,000      | Sequential Sum         | 4,953,224   | 0         |
| 100,000      | Parallel Sum (Threads) | 4,953,224   | 181       |
| 100,000      | Parallel Sum (LINQ)    | 4,953,224   | 132       |
| 1,000,000    | Sequential Sum         | 49,482,879  | 1         |
| 1,000,000    | Parallel Sum (Threads) | 49,482,879  | 169       |
| 1,000,000    | Parallel Sum (LINQ)    | 49,482,879  | 155       |
| 10,000,000   | Sequential Sum         | 495,136,452 | 17        |
| 10,000,000   | Parallel Sum (Threads) | 495,136,452 | 476       |
| 10,000,000   | Parallel Sum (LINQ)    | 495,136,452 | 158       |

## Conclusion

The benchmark results clearly show the performance differences between the sequential and parallel processing approaches to summing large arrays. While parallel methods generally offer better performance for larger arrays, the overhead involved in smaller arrays can result in slower performance compared to sequential processing.
